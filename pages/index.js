import Head from 'next/head';

import styles from '../styles/Home.module.css';
import Vendor from "../Components/Vendors";

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>E-Cart</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <div className={'py-5'}>
          <h1 className={styles.title}>
            Welcome to E-Cart
          </h1>
        </div>
        <Vendor/>
      </main>
    </div>
  )
}
