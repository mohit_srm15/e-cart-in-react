import React from 'react';
import {Row , Col , Container } from 'react-bootstrap';
import Router from "next/router";

import Color from "../styles/Color";

function Vendor(){

  let  data = require('../Constants/task');

  function handleVendorClick(storeInfo){
    localStorage.setItem('storeInfo' , JSON.stringify(storeInfo));
    Router.push({pathname: "/Products"});
  };

  return(
    <div>
      <Container>
        <Row>
          {
            data.data && data.data.length > 0
              ? data.data.map((storeInfo, i) => {
                return (
                  <Col xs={4}
                       className={'my-3'}
                       key={`vendor-${i}`}
                       onClick={()=> handleVendorClick(storeInfo)}
                  >
                    <div className={'vendorCard'}>
                      <Row>
                        <Col xs={4}>
                          <img
                            src={ storeInfo.store_image
                              ? storeInfo.store_image
                              :
                              "/images/store_image.jpg"
                            }
                            className={'store_image'}
                          />
                        </Col>
                        <Col xs={8} className={'vendor_info'} >
                          <div >
                            <h6  style={{color: Color.gray02}}>
                              {storeInfo.name ? storeInfo.name : 'N/A'}
                            </h6>
                          </div>
                          <Row>
                            <Col xs={2}>

                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </div>
                  </Col>
                )
              })
              :
              null
          }
        </Row>
      </Container>
    </div>
  )
}

export default Vendor;
