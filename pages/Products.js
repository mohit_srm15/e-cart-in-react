import React,{useEffect , useState} from 'react';
import {Row , Col , Container ,  Tabs , Tab} from 'react-bootstrap';

import {CONSTANTS} from "../Constants/Constants";
import styles from "../styles/Home.module.css";
import Color from "../styles/Color";
import ProductItem from "../Components/ProductItem";

function Products(){

  let {PRODUCT_CATEGORIES, CATEGORY_VARIETY, ITEMS_LIST} = CONSTANTS;

  const [storeInfo , setStoreInfo] = useState({});
  const [activeCategory, setActiveCategory] = useState('fruitsAndVegetables');

  let activeVariety = {
    label: 'Food Grains',
    value: 'foodGrains',
  };

  useEffect(()=>{
     let data = localStorage.getItem('storeInfo');
     setStoreInfo(JSON.parse(data));
  } , [0]);


  return(
    <Container>
      <main className={styles.main}>
        <div>
          <h3 style={{justifyContent: 'center' , alignItems: 'center' , display: 'flex', color: Color.gray04}}>
            {`Welcome to ${storeInfo.name  ? storeInfo.name : 'N/A'}`}
          </h3>
        </div>
      </main>
      <Row>
        {
          Object.keys(PRODUCT_CATEGORIES).map(item => {
            return(
              <Col
                xs={3}
                style={{cursor: 'pointer'}}
                onClick={()=> setActiveCategory(item)}
              >
                <div className={'product_variety'}>
                  <Row  style={{justifyContent: 'center' ,alignItems: 'center' , display: 'flex'}}>
                    <img
                      src={PRODUCT_CATEGORIES[item].image}
                      style={{height: '-webkit-fill-available' , width: '-webkit-fill-available'}}
                    />
                  </Row >
                </div>
                <Row
                  style={{
                    justifyContent: 'center' ,
                    alignItems: 'center' ,
                    display: 'flex'
                  }}
                  className={'my-2'}
                >
                  <p style={{color: activeCategory=== item ? Color.gray04 : Color.gray02 }} >
                    {PRODUCT_CATEGORIES[item].label}
                  </p>
                </Row>
              </Col>
            )
          })
        }
      </Row>
      <main className={'my-3'} >
        <div >
          <h3 style={{color: Color.gray04}}>
            {`Categories`}
          </h3>
        </div>
      </main>
      <Row className={'my-2'}>
        <Tabs  id="uncontrolled-tab-example" defaultActiveKey={activeVariety}>
          {
            CATEGORY_VARIETY.map((item , i)=> {
              return(
                <Tab eventKey={item.value} title={item.label} key={`tab-${i}`} >
                </Tab>
              )
            })
          }
        </Tabs>
      </Row>
      <Row>
        {
          Object.keys(ITEMS_LIST).map((productItem , i)=> {
            return(
              <Col key={`product-${i}`} xs={6} className={'py-3 px-5'}>
                <ProductItem productInfo={ITEMS_LIST[productItem]}/>
              </Col>
            )
          })
        }
      </Row>
    </Container>
  )
}

export default  Products;
