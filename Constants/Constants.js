export const CONSTANTS = {
  PRODUCT_CATEGORIES: {
    fruitsAndVegetables: {
      label: 'Fruits & Vegetables',
      image: '/images/fruits.jpg',
    },
    groceryProducts: {
      label: 'Grocery Products',
      image: '/images/grocery.jpg',
    },
    dairyProducts: {
      label: 'Dairy Products',
      image: '/images/dairy_products.jpg',
    },
    snacks: {
      label: 'Snacks & Chocolates',
      image: '/images/snacks.jpg',
    },
  },
  CATEGORY_VARIETY: [
    {
      label: 'Food Grains',
      value: 'foodGrains',
    },
    {
      label: 'Pulses',
      value: 'pulses',
    },
    {
      label: 'Flours',
      value: 'flours',
    },
    {
      label: 'Edible Oils',
      value: 'oil',
    },
  ],
  ITEMS_LIST: {
    fortuneOil_1: {
      label: 'Fortune Oil',
      description: 'Fortune everyday Basmati rice Full Day',
      quantity: '5kg',
      veg: true,
      image: '/images/fortune_oil.jpg',
      rate: '149'
    },
    fortuneOil_2: {
      label: 'Fortune Oil',
      description: 'Fortune everyday Basmati rice Full Day',
      quantity: '10kg',
      veg: true,
      image: '/images/fortune_oil.jpg',
      rate: '709'
    },
    basmatiRice_1: {
      label: 'Basmati Rice',
      description: 'Kohinoor authentic Royale Basmati rice',
      quantity: '0.5kg',
      veg: true,
      image: '/images/basmati_rice.jpg',
      rate: '190'
    },
    basmatiRice_2: {
      label: 'Basmati Rice',
      description: 'Kohinoor authentic Royale Basmati rice',
      quantity: '5kg',
      veg: true,
      image: '/images/basmati_rice.jpg',
      rate: '1490'
    },
  },
};
