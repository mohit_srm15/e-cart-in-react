import React from 'react';
import {Row, Col , Button} from 'react-bootstrap';

function ProductItem(props){

  let {productInfo}  = props;
  return(
    <div>
      <Row className={'product_card'}>
        <Col
          xs={3}
          style={{
            justifyContent: 'center' ,
            alignItems: 'center' ,
            display: 'flex'
          }}
        >
          <img
            src={productInfo.image}
            style={{height: 'auto' , width: '-webkit-fill-available'}}
          />
        </Col>
        <Col xs={9}>
          <Row>
            <div className={'d-flex'}>
              <div className={'veg_symbol_container'} >
                <div className={'product_veg_symbol'} />
              </div>
              <div className={'mx-2'}>
                <p>{`${productInfo.description} - ${productInfo.quantity}`}</p>
              </div>
            </div>
          </Row>
          <Row style={{justifyContent: 'space-between' ,alignItems: 'center'}}>
            <p>{`$${productInfo.rate}`}</p>
            <button className={'add_button'}>
              Add
            </button>
          </Row>
        </Col>
      </Row>
    </div>
  )
}

export default ProductItem;
